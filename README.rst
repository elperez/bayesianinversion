++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
README file for BayesianInversion E.Perez
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

This repository belongs to Elijah Perez. 

This repository contains all code and Latex files associated with Elijah Perez's thesis on "Optimal Transport Driven Bayesian Inversion with Application to Signal Processing."

To view final draft of thesis open file labled "OTBayesianInversion.pdf" that is a stand alone file (not in a folder) 
